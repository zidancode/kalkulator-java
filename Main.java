/* 
    Author: Zidan Herlangga
*/

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        while (true) {
            try {
                Scanner scanner = new Scanner(System.in);
                int pilih;
                double n1, n2;
                System.out.println("\n== Kalkulator Java Sederhana ==\n55");
                System.out.println("1. Penjumlahan  (+)");
                System.out.println("2. Pengurangan  (-)");
                System.out.println("3. Perkalian    (*)");
                System.out.println("4. Pembagian    (/)");
                System.out.println("5. Modulus      (%)");

                System.out.print("\nPilih no berapa? ");
                pilih = scanner.nextInt();

                System.out.print("\nMasukan bilangan awal: ");
                n1 = scanner.nextDouble();

                System.out.print("Masukan bilangan akhir: ");
                n2 = scanner.nextDouble();

                switch (pilih) {
                    case 1:
                        System.out.print("\nHasil: " + Math.round(n1 + n2));
                        break;
                    case 2:
                        System.out.print("\nHasil: " + Math.round(n1 - n2));
                        break;
                    case 3:
                        System.out.print("\nHasil: " + Math.round(n1 * n2));
                        break;
                    case 4:
                        if (n2 != 0) {
                            System.out.print("\nHasil: " + (n1 / n2));
                            break;
                        } else {
                            System.out.print("\nTidak bisa di bagi 0");
                        }
                    case 5:
                        System.out.print("\nHasil: " + (n1 % n2));
                        break;
                    default:
                        break;
                }
                scanner.close();
            } 
            catch (Exception e) {
                break;   
            }
        }
    }
}